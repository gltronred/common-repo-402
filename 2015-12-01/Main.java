
import java.io.*;
import java.net.*;
import java.util.*;

public class Main {
    public static void work(List<Integer> primes) throws IOException {
	ServerSocket ss = new ServerSocket(4567);
	Map<String, Integer> records = new HashMap<>();
	while(true) {
	    new ServerThread(ss.accept(),primes,records,System.out).start();
	}
    }
    public static void main(String[] args) throws IOException {
	List<Integer> primes = new LinkedList<>();
	new PrimeThread(primes).start();
	work(primes);
    }
}

