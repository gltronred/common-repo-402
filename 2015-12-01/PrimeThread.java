
import java.util.*;

public class PrimeThread extends Thread {
    private List<Integer> primes;
    public PrimeThread(List<Integer> primes) {
	this.primes = primes;
    }
    public void run() {
	primes.add(2);
	int x = 3;
	try {
	    while (true) {
		boolean p = true;
		ListIterator<Integer> it = primes.listIterator();
		int i = 1;
		while (p && i*i <= x && it.hasNext()) {
		    i = it.next();
		    p &= x%i != 0;
		}
		if (p) {
		    primes.add(x);
		}
		Thread.sleep(10);
		x+=2;
	    }
	} catch (InterruptedException e) {
	    System.out.println("Oops! " + e);
	}
    }
}

