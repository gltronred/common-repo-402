
import java.io.*;
import java.net.*;
import java.util.*;

public class ServerThread extends Thread {
    private Scanner sc;
    private PrintWriter pw;
    private List<Integer> primes;
    private Map<String,Integer> records;
    private String name;
    private PrintStream out;
    public ServerThread(Socket s, List<Integer> primes, Map<String,Integer> records, PrintStream out) throws IOException {
	sc = new Scanner(s.getInputStream());
	pw = new PrintWriter(s.getOutputStream());
	this.primes = primes;
	this.records = records;
	pw.println("Your name: "); pw.flush();
	name = sc.nextLine();
	this.out = out;
    }
    public void run() {
	Random random = new Random();
	int score = 0;
	while (true) {
	    Integer lastPrime = primes.get(primes.size()-1);
	    int randomNumber;
	    do {
		randomNumber = random.nextInt(lastPrime - 1) + 2;
	    } while (randomNumber % 2 == 0 || randomNumber % 3 == 0 || randomNumber % 5 == 0);
	    pw.println("Is " + randomNumber + " prime?");
	    pw.flush();
	    boolean trueResult = primes.contains(randomNumber);
	    String s;
	    do {
		s = sc.nextLine();
	    } while (!s.equals("y") && !s.equals("n"));
	    boolean guess = s.equals("y");
	    if (trueResult == guess) {
		pw.println("You are right!");
		score += (randomNumber / 100 + 1);
	    } else {
		pw.println("You are wrong");
		score -= (randomNumber / 1000 + 1);
	    }
	    pw.println("Score is " + score);
	    if (records.get(name) == null || score > records.get(name)) {
		records.put(name,score);
		pw.println("=== RECORDS! ===");
	        for (Map.Entry<String,Integer> e : records.entrySet()) {
		    pw.println("> " + e.getKey() + ": " + e.getValue());
		}
		out.println("NEW RECORD!!! " + name + ": " + score);
	    }
	    pw.flush();
	}
    }
}

